# Twitter Profile

## Context

Learn the basics of Redux through writing a small Twitter app, using only vanilla JavaScript.

## The Assignment

You’ll need to create everything: Action Creators, Actions, Reducers. To do this you are not alone, using Test-Driven Development you’ll acomplish this task.

### Setup Instructions

In Terminal:

```sh
# (1) Clone the repo
$ git clone https://gitlab.com/agzeri/redux-intro

# (2) Go to folder
$ cd redux-intro

# (3) Install dependencies
$ npm install
```

### Test-Driven Development

**DON’T CHANGE THE TEST CODE, YOU ONLY NEED TO UNSKIP THEM.**

#### How to

+ In your terminal, run `npm test`
+ If you are not seeing anything, do next:
  + Remove the `x` from tests.

Example:

From:

```js
xtest("", () => {});
```

To:

```js
test("", () => {});
```

## Deliverables

### Sprint 1 | Import `Redux`

**Summary**

Add `redux` library to your project.

**Instructions**

+ Install `redux` library.
+ To test your work, use `tests/redux.test.js`.

### Sprint 2 | Define Action Creators for Tweets

**Summary**

We want to have a list that includes our tweets. For this, you need to create the structure for these actions.

##### User Story. A User can perform:

|#|Action|Description|
|-|-|-|
|1|Create a Tweet|Id, Text, Date, Username|
|2|Edit a Tweet|Id, Text|
|3|Delete a Tweet|Id|

**Instructions**

+ Create a function(or functions) that will return an object containing those characteristics.
+ To test your work, check `tests/actions/`. There are three files, start with `create_tweet.test.js`, then `edit_tweet.test.js` and finally `delete_tweet.test.js`.


### Sprint 3 | Reducer

**Summary**

To make changes in our information, create a reducer that takes full resposibility of Tweets.

**Instructions**

+ You need to validate each case (Create a Tweet, Edit a Tweet, Delete a Tweet).
+ Apply the needed logic in each case. Remember, it’s doesn’t work if you are mutating data.
+ Don’t forget to return the base case, and to initialize the piece of data that your are changing.

You can test the reducers with `tests/reducers/tweet.test.js` file.

### Sprint 4 | Store

**Summary**

Integrate everything in the store so we can dispatch actions.

**Instructions**

+ Use `createStore` function to create our store.
+ Use `combineReducers` to merge our reducer function.
+ To validate your code, use `tests/store/tweets.test.js` file.

### Sprint 5 | Integrate it with the UI

**Summary**

Add a basic interface to create and remove Tweets.

**Instructions**

+ Create a pen from Codepen.io, activate Babel and add redux as library.
+ Put all your source code in the JS panel. Copy and paste, expect `module.exports...`, we don’t need it.
+ Use a <form> with a <textarea> and <input type="submit"> to create a Tweet.
+ Take advantage of `submit` event.
+ Print all Tweets created from form.
+ Add a button to each tweet so we can delete them.

**Design**

Free.

### Sprint 6 | Pinned Tweet (Action Creator, Action, Reducer)

**Summary**

Implement this functionality in code, add the ability to each user so they can pin a tweet. You need to save only the tweet id.

##### User Story

|#|Description|
|-|-|
|1|Pin a Tweet|
|2|Remove the pinned Tweet|

**Instructions**

+ You need to create an action creator.
+ Think if you need to create another reducer or use the same as Tweets.
+ Don’t forget to add it into the reducers if you decide to create a new one.

**There are no tests to check your work, otherwise you’ll see the actual answer.**

### Sprint 7 | User Historial

**Summary**

We want to save every action a user has performed in their timeline.

##### User Story

|#|Action|Description|
|-|-|-|
|1|Create a Tweet|This use has created a tweet.|
|2|Edit a Tweet|This user has editer a tweet.|
|3|Delete a Tweet|This user has deleted a tweet.|
|4|Pin a Tweet|This user has pinned a tweet.|
|5|Remove a pinned Tweet|This user has remove a pinned tweet.|

**Instructions**

+ You need to create an action creator.
+ Think about how you need to organize these actions, **what kind of data type you will need?** and **What you will save?**.
+ Don’t forget to add it into the reducers if you decide to create a new one.

_**NOTE. Think about the next situation: What would happen if a pinned tweet is removed?**_
