const createTweet = (tweet, username) => {
  return {
    type: "CREATE_TWEET",
    payload: {
      id: tweet.id,
      username: username,
      text: tweet.text,
      createdAt: new Date()
    }
  }
}

const editTweet = (id, text) => {
  return {
    type: "EDIT_TWEET",
    payload: {
      id: id,
      text: text,
      updatedAt: new Date()
    }
  }
}

const deleteTweet = (id) => {
  return {
    type: "DELETE_TWEET",
    payload: {
      id: id
    }    
  }
}

module.exports = { 
  createTweet: createTweet,
  editTweet: editTweet,
  deleteTweet: deleteTweet
};