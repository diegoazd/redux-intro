const tweets = (db = [], action) => {
  if(action.type === 'CREATE_TWEET') {
    return [...db,
      action.payload
    ];
  }else if(action.type === 'EDIT_TWEET') {
    const payload = action.payload;
    const index = db.findIndex(e => e.id === action.payload.id);
    db[index].text = payload.text;
    return [...db];
  }else if(action.type === 'DELETE_TWEET') {
    return db.filter(e => e.id !== action.payload.id);
  }

  return db;
};

module.exports = {tweets: tweets};
