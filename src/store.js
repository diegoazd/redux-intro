const { createStore, combineReducers } = require('redux');
const reducers = require("./reducers");
const actions = require("./action_creators");

const tweets = combineReducers({
  tweets: reducers.tweets,
});

const store = createStore(tweets);
module.exports = store;